; Straightforward function to replace an image in an ImageList given an image file path.
; Primarily needed for LLB (Link List Builder)
; � Drugwash, June 2015-Jan 2017
; GdiPlus Startup and Shutdown adapted for AHK_L from Gdip library wrapper 1.44 by tic (Tariq Porter)
;================================================================
/*
; EXAMPLE
#NoEnv
#SingleInstance, Force
ListLines, Off
SetControlDelay, -1
SetWinDelay, -1
SetBatchLines, -1
ListLines, Off
StringCaseSense, On
SetFormat, Integer, D
DetectHiddenWindows, On
CoordMode, Mouse, Relative
#include ..\updates.ahk
#include ..\func_ImageList.ahk

file1=%A_ScriptDir%\err.png

pToken := Gdip_Startup()
Gui, Add, ListView, w200 h200, name
hIL := ILC_Create(1, 1, 96, "M32")
LV_SetImageList(hIL, 1)
if !r := IL_Replace(hIL, file1, "1", 0xFFFF00FF)
	msgbox, no icon replacement
else LV_Add("Icon1", "test")
e := Gdip_Shutdown(pToken)
Gui, Show
OnExit, cleanup
return

cleanup:
IL_Destroy(hIL)
GuiClose:
ExitApp
*/
;================================================================
IL_Replace(hIL, file, idx=1, bkg=0xFFFFFFFF)
;================================================================
{
Global w9x, Ptr, PtrP
Static fileW
if !A_IsUnicode
	{
	VarSetCapacity(fileW, 1024, 0)
	DllCall("kernel32\MultiByteToWideChar"
		, "UInt"	, 0
		, "UInt"	, 0
		, Ptr		, &file
		, "Int"	, -1
		, Ptr		, &fileW
		, "Int"	, 512)
	DllCall("gdiplus\GdipCreateBitmapFromFile", Ptr, &fileW, PtrP, pBitmap)
	VarSetCapacity(fileW, 0)
	}
else DllCall("gdiplus\GdipCreateBitmapFromFile", Ptr, &file, PtrP, pBitmap)
if !pBitmap
	outputdebug, error in %A_ThisFunc%(): no pBitmap!
DllCall("ImageList_GetIconSize", Ptr, hIL, PtrP, iW, PtrP, iH)
DllCall("gdiplus\GdipGetImageThumbnail"
	, Ptr		, pBitmap		; Image
	, "UInt"	, iW			; thumbWidth
	, "UInt"	, iH			; thumbHeight
	, PtrP	, pThumb		; thumbImage
	, Ptr		, 0			; callback
	, "UInt"	, 0)			; callbackData
DllCall("gdiplus\GdipCreateHBITMAPFromBitmap", Ptr, pThumb, PtrP, hBitmap, "UInt", bkg)
DllCall("gdiplus\GdipDisposeImage", Ptr, pBitmap)
DllCall("gdiplus\GdipDisposeImage", Ptr, pThumb)
if !hBitmap
	outputdebug, error in %A_ThisFunc%(): no hBitmap!
; add to image list
if !(r := DllCall("ImageList_AddMasked", Ptr, hIL, Ptr, hBitmap, "UInt", bkg)+1)
	r := DllCall("ImageList_Add", Ptr, hIL, Ptr, hBitmap, "UInt", 0)+1
DllCall("DeleteObject", Ptr, hBitmap)
return r
}
;================================================================
Gdip_Startup()
;================================================================
{
Global AW, Ptr, PtrP
if !DllCall("GetModuleHandle" AW, "Str", "gdiplus")
	DllCall("LoadLibrary" AW, "Str", "gdiplus")
VarSetCapacity(si, 16, 0), si := Chr(1)
DllCall("gdiplus\GdiplusStartup", PtrP, pToken, Ptr, &si, Ptr, 0)
return pToken
}
;================================================================
Gdip_Shutdown(pToken)
;================================================================
{
Global AW, Ptr
DllCall("gdiplus\GdiplusShutdown", Ptr, pToken)
if hModule := DllCall("GetModuleHandle" AW, "Str", "gdiplus")
	return !DllCall("FreeLibrary", Ptr, hModule)
}
