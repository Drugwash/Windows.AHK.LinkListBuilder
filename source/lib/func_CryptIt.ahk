;==VERSIONING==>1.1
;About:	MD5, SHA1, RSA crypto
;Compat:	B1.0.48.05, L1.1.20.0
;Depend:	updates.ahk,func_WC2MB.ahk
;Rqrmts:
;Date:	2017.01.02 15:36
;Version:	1.6
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

#include updates.ahk
/*
; EXAMPLE - uncomment to run
FileRead, strg, install.rdf
r1 := CryptIt(strg, "MD5")
e1 := Base64encodeH(r1)
r2 := CryptIt(strg, "SHA")
e2 := Base64encodeH(r2)
msgbox, MD5: %r1%`nSHA: %r2%`n`nD1: %e1%`nD2: %e2%
return
#include func_Base64.ahk
*/

CryptIt(ByRef data, typ="", useW=TRUE, retW=FALSE)
{
Global AW, Ptr, PtrP, AStr, A_CharSize
len := VarSetCapacity(data, -1)//A_CharSize
if (len != StrLen(data))
	len := StrLen(data)	; Sort of a fallback
if (!len OR DllCall("IsBadReadPtr", Ptr, &data, "UInt", len))
	return 0
if (!useW && A_IsUnicode)
	WC2MB(&data, data2)
else if (useW && !A_IsUnicode)
	MB2WC(data, data2:=0)
else data2 := data
CALG := typ="MD5" ? 0x8003 : typ="SHA" ? 0x8004 : 0x2400	; defaults to RSA
res=0							; initialize result
hPro=0
hHash=0
API = CryptAcquireContextA
if !DllCall("advapi32\" API
		, PtrP	, hPro			; phProv
		, Ptr		, NULL			; pszContainer
		, Ptr		, NULL			; pszProvider
;		, "UInt"	, 1				; dwProvType PROV_RSA_FULL
		, "UInt"	, 24				; dwProvType PROV_RSA_FULL
;		, "UInt"	, 0)				; dwFlags CRYPT_NEWKEYSET = 0x00000008
		, "UInt"	, 0xF0000000)				; dwFlags CRYPT_NEWKEYSET = 0x00000008
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	return res
	}
API=CryptCreateHash
if !DllCall("advapi32\" API
		, Ptr		, hPro			; hProv
		, "UInt"	, CALG			; CALG_SHA/MD5/RSA
		, Ptr		, 0				; hKey
		, "UInt"	, 0				; dwFlags
		, PtrP	, hHash)			; phHash
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	goto CryptIt_exit1
	}
API=CryptHashData
s := len*2**(useW=TRUE)
if !DllCall("advapi32\" API
		, Ptr		, hHash			; hHash
		, Ptr		, &data2			; pbData
		, "UInt"	, s				; dwDataLen
		, "UInt"	, 0)				; dwFlags
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	goto CryptIt_exit2
	}
sz=4
val=
API=CryptGetHashParam
if !DllCall("advapi32\" API
	, Ptr		, hHash				; hHash
	, "UInt"	, 0x4				; dwParam HP_HASHSIZE
	, PtrP	, val					; pbData
	, PtrP	, sz					; pdwDataLen
	, "UInt"	, 0)					; dwFlags
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	goto CryptIt_exit2
	}
else
	{
	len := val
	VarSetCapacity(buf, val, 0)
	if !DllCall("advapi32\" API
			, Ptr		, hHash	; hHash
			, "UInt"	, 0x2	; dwParam HP_HASHVAL
			, Ptr		, &buf	; pbData
			, PtrP	, len		; pdwDataLen
			, "UInt"	, 0)		; dwFlags
		MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	else
		{
		res2=
		ofi := A_FormatInteger
		SetFormat, Integer, H
		Loop, %len%
			res2 .= SubStr("00" SubStr(NumGet(buf, A_Index-1,"UChar"), 3), -1)
		if (A_IsUnicode && !retW)
			WC2MB(&res2, res)
		else res := res2
		VarSetCapacity(res2, 0)
		StringUpper, res, res
		SetFormat, Integer, %ofi%
		}
	}
CryptIt_exit2:
API=CryptDestroyHash
if hHash
	DllCall("advapi32\" API, Ptr, hHash)
else MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
CryptIt_exit1:
API=CryptReleaseContext
if hPro
	DllCall("advapi32\" API, Ptr, hPro, "UInt", 0)
else MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
VarSetCapacity(data2, 0)
return res
}
#include func_WC2MB.ahk
