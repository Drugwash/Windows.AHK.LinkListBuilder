; Enumerates installed and registered browsers
; � Drugwash 2015
/*
r := EnumBrowsers(list, def)
msgbox, %r% installed browsers.`nDefault is %def%.`n%list%
return
*/

EnumBrowsers(ByRef blist, ByRef bdef)
{
Global AW, Ptr, PtrP, AStr
if DllCall("GetProcAddress"
		, Ptr, DllCall("GetModuleHandle" AW, "Str", "kernel32.dll")
		, AStr, "IsWow64Process") ? "Wow6432Node\" : ""
	DllCall("IsWow64Process", Ptr, DllCall("GetCurrentProcess", Ptr), PtrP, r)
x64 := r ? "Wow6432Node\" : ""
regInet := "Software\" x64 "Clients\StartMenuInternet"
blist=
RegRead, i, HKCU, %regInet%							; default browser
RegRead, bdef, HKLM, %regInet%\%i%					; default browser
if !bdef
	RegRead, bdef, HKLM, %regInet%\%i%, LocalizedString	; default browser
RegRead, u, HKLM, %regInet%							; default browser
RegRead, ubdef, HKLM, %regInet%\%u%					; default browser
if !bdef
	{
	bdef := ubdef
	i := u
	}
Loop, HKLM, %regInet%, 2, 0
	{
	RegRead, bname, %A_LoopRegKey%, %A_LoopRegSubKey%\%A_LoopRegName%
	if !bname
		{
		RegRead, bname, %A_LoopRegKey%, %A_LoopRegSubKey%\%A_LoopRegName%, LocalizedString
		StringLeft, bname, bname, % InStr(bname, "\", FALSE, 0)-1
		StringTrimLeft, bname, bname, % InStr(bname, "\", FALSE, 0)
		}
	RegRead, bpath, %A_LoopRegKey%, %A_LoopRegSubKey%\%A_LoopRegName%\shell\open\command
	StringReplace, bpath, bpath, ",, All
	SplitPath, bpath, bexe, bpath
	blist .= bname "|" bexe "|" bpath "`n"
	if (A_LoopRegName=i)
		bdef .= "|" bexe
	}
StringTrimRight, blist, blist, 1
Sort, blist, CL U
bidx=0
Loop, Parse, blist, `n, `r
	{
	if InStr(A_LoopField, bdef "|")
		bdef := A_Index
	bidx++
	}
return bidx
}
