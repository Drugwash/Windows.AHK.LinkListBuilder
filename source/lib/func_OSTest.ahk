; A simple function to compare OS version in a flexible and easy way
; � Drugwash, June 2015

; EXAMPLE (uncomment to run)
/*
r := OSTest("ME", "Lo") ? "true" : "false"
msgbox, OS version comparison is %r%
return
*/

OSTest(nm, cin="E")
{
Static t="NT4,95,98,ME,2000,XP,2003,VISTA,7,8,8.1"
StringLeft, cond, cin, 2
StringUpper, name, nm
if name not in %t%
	{
	MsgBox, 0x2010, %A_ThisFunc%(), Bad version name: %nm% ! Allowed are:`n%t%
	return
	}
Loop, Parse, t, CSV
	{
	if (name=A_LoopField)
		m := A_Index
	if (A_OSVersion=A_LoopField)
		c := A_Index
	}
if cond in E,Eq			; Equal
	return (m=c ? TRUE : FALSE)
else if cond in EL,LE	; Equal or Lower
	return (c<=m ? TRUE : FALSE)
else if cond in L,Lo		; Lower
	return (c<m ? TRUE : FALSE)
else if cond in EH,HE	; Equal or Higher
	return (c>=m ? TRUE : FALSE)
else if cond in H,Hi		; Higher
	return (c>m ? TRUE : FALSE)
else MsgBox, 0x2010, %A_ThisFunc%(),
	(LTrim
	Bad condition string: %cin% !
	Allowed are:
	E(qual`)
	L(ower`)
	H(igher`)
	EL/LE (equal or lower`)
	EH/HE (equal or higher`)
	)
return
}
