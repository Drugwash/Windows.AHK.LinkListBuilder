![](ss_llb2404.png)

### ABOUT

This is basically a collection of e-mails and links.        
Starting with v**2.4.0.0** it can also include personal info about the people.      
To protect their privacy please save the database(s) in encrypted form      
by providing a password when required by the application.       
Each database uses its own password so make sure you don't forget or mix them.

Intended usage is for bloggers that want to keep an offline list of 'favorites'     
and it facilitates advertising (in a good sense!) their blogger friends by      
automatically building HTML-formatted lists of blogs (or e-mails) that can be       
published in blog posts.

**This application is in no way intended for spamming or		
any other malevolent actions!**

### INSTALLATION

No installation is required - the application runs from where it's deployed,        
provided the path has the necessary write permissions.      
It is written for portability so all its settings and dependencies are found        
in the application folder. Therefore it can be run from USB sticks and similar.

Unpack the archive with full paths into a folder with write permissions.        
At least on Windows 2000 or later, avoid Program Files and Windows as deploy location.

### Windows® 9x KernelEx users

For unknown reasons, on my 98SE system the application requires     
specific 98SE KernelEx mode (right-click **> Properties > KernelEx**).      
In default mode or 2000/XP mode it crashes trying to convert JPG into ICO       
for the image list, so make sure you set the proper mode before running it.

### REMARKS

- Avatars are based on the e-mail addresses.      
For avatar retrieval, this application connects to Gravatar.com,        
so you may have to allow it in your firewall's settings.        
Avatars will only be retrieved once for each new entry,     
but they can be refreshed manually at any time using the **Avatars** button.

- It is recommended that you fill in all four basic fields for each entry,        
because different types of output require different data.       
The **Real Name** field is what the list is sorted by. If you do not know it      
or do not want to set it, it will be automatically taken from the **Nickname** field.       
Therefore either **Nickname** or **Real Name** must be filled in for correct operation.

- There is no undo for deleting an entry so be careful!

- Don't forget to select the desired entries before building a list! ;-)      
This can be done item by item or by using the  **All**, **None**, **Invert** buttons.

### FINAL NOTE

This application is freeware/open-source, written in AutoHotkey.        
It comes with no guarantee or warranty whatsoever.      
The author (Drugwash) is not responsible for the use or misuse      
of this application. Please exercise common-sense when using it!

Enjoy! :-)

**© Drugwash, 2015-2023**
